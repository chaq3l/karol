
const invoiceEditor = angular.module('InvoiceEditor', [])
const app = angular.module('app', ['InvoiceEditor'])

// app.run(function ($rootScope){ 
//   $rootScope.restartInvoice = true

// })

invoiceEditor.controller('InvoiceEditorCtrl', ($scope) => {
  
  var invoice = {
    positions: [
      {
        id: "123",
        title: 'Pozycja 123',
        netto: 1000,
        tax: 23,
        brutto: 1230
      }
    ], 
    summary : {
      total: 0
    }
  }
  $scope.invoice = invoice
  $scope.total = 0 
  
  $scope.getTotal = () => {
    $scope.total = 0 
    const pos = invoice.positions
    invoice.summary = 0
    for(position of pos){
      console.log(  $scope.total)
      $scope.total += Number(position.brutto)
    }
    invoice.summary.total = Number($scope.total)
  }

  $scope.addPosition = (index) => {  
    var newIdNumber = String(Number(invoice.positions[0].id)+(Object.keys(invoice.positions).length))    
    
    $scope.invoice.positions.splice(index + 1, 0, {
    id: newIdNumber,
    title: ('Pozycja'+ String(newIdNumber)),
    netto: 1000,
    tax: 23,
    brutto: 1230
  }),   
  $scope.invoice = invoice    
}
  $scope.removePosition = (index) => { $scope.invoice.positions.splice(index, 1) }
  
})

angular.bootstrap(document, ['app'])
