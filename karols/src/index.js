// console.log('Hello Angular')
// console.log(angular.version)
angular.module('config', [])
angular.module('myApp', ['config', 'tasks', 'users', 'settings', 'task.service'])
const app = angular.module('myApp')

app.run(function ($rootScope, PAGES) { })

//app.run(function ($rootScope){ })
app.constant('PAGES', [
    { name: 'users', label: 'Users', tpl:'views/users.html' },
    { name: 'tasks', label: 'Tasks', tpl:'views/tasks.html' },
    { name: 'login', label: 'Login', tpl:'views/login.html' },
    { name: 'settings', label: 'Settings', tpl: 'views/settings.html'}
])

app.constant('INITIAL_TASKS', [{ name: 'task1', id: 1 }, { name: 'task2', id: 2 }, { name: 'task3', id: 3 }])

app.controller('AppCtrl', ($scope, PAGES) => {
    $scope.title = 'MyApp'
    $scope.user = {name: 'Guest'}
    //$scope.user = 'Guest'

    $scope.isLoggedIn = false;
    $scope.login = () => {
        $scope.user.name = 'Admin'
        $scope.isLoggedIn = true
    }
    
    $scope.logout = () => {
        $scope.user.name = 'Guest'
        $scope.isLoggedIn = false
    }

    $scope.pages = PAGES

    $scope.currentPage = $scope.pages[0]

    $scope.changedisplay = pageName => {
        $scope.currentPage = $scope.pages.find(p => p.name === pageName.toString())
        //console.log( $scope.currentPage)
      }

    // $scope.displayTasks = true
    // $scope.displayLogin = false
    // $scope.changedisplay = (displayDescription) => {
    //     switch(displayDescription){
    //         case 'Tasks':
    //             $scope.displayTasks = true
    //             $scope.displayUsers = false
    //             $scope.displayLogin = false
    //             break

    //         case 'Users':
    //             $scope.displayUsers = true
    //             $scope.displayTasks = false
    //             $scope.displayLogin = false
    //             break
    //         case 'Login':
    //             $scope.displayUsers = false
    //             $scope.displayTasks = false
    //             $scope.displayLogin = true
    //     }
    // } 
 


})
{/* <body ng-app=myApp>  */}
angular.bootstrap(document, ['myApp'])