angular.module('settings', [])
.controller('SettingsCtrl', ($scope) => {
    const vm = $scope.settings = {}
    vm.test = 'tittle tittle tittle '
    
})

angular.module('settings').directive('appHighlight', function(PAGES){
    /**@type {require('angular').IDirectiveFactory} */
    return {
        restrict: 'EACM', //Element | Attr | Class | coMment
        link(scope, $element, attrs, ctrl){
            console.log('Hello', $element)
            $element.css('color', 'red')
            .html('<p>placki&nbsp; &raquo;</p>')
            .on('click', event => {
                //angular.element(event.currentTarget) // zamienne z linią poniżej
                $element                                // zamienne z linią powyżej
                .find('p')[0].classList.toggle('text-success')          // zamienne z linią poniżej
                //.css('color', 'blue')                                 // zamienne z linią powyżej
            })
        }
    }
})
.directive('appAlert', function(){
    return {
        transclude: true,
        scope:{
            message: "@",
            type: "@",
            onDismiss: "&"
        },
        template: /* html */ `
        <div class="alert alert-{{type}}" ng-if="$ctrl.displayAlert">
            <span class="close float-end" ng-click="$ctrl.close()">&times;</span>
            <div>
                {{message}}
            </div>
            <ng-transclude>Test</ng-transclude>
        </div>
        `,
        controller($scope){
            const vm = $scope.$ctrl = {}
            vm.displayAlert = true
            vm.close = () =>{
                vm.displayAlert = !vm.displayAlert
                $scope.onDismiss()
            } 
        }
    }
})