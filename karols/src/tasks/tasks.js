
const tasks = angular.module('tasks', [])

angular.module('tasks')
  .component('tasksPage', {
    template:/* html */`<div> 
    TasksPage {{$ctrl.title}}
        <div class="row">
          <div class="col">
            <tasks-list tasks="$ctrl.tasks" 
                        on-select="$ctrl.select($event)"></tasks-list>
          </div>
          <div class="col">
            <task-details task="$ctrl.selected" on-cancel="$ctrl.cancelSelect($event)" on-edit="$ctrl.edit($event)" ng-if="$ctrl.selected && $ctrl.mode ==='details'">
            </task-details>

            <task-editor task="$ctrl.selected" on-save="$ctrl.save($event)" on-cancel="$ctrl.cancelEdition($event)" ng-if="$ctrl.selected && $ctrl.mode ==='edit'"></task-editor>
          </div>
        </div>
      </div>`,
    controller(INITIAL_TASKS, TaskService) {
      this.$onInit = () => {
        this.title = 'Tasks'
        this.mode = 'details'
        this.selected = null
        this.tasks = INITIAL_TASKS        
      }
      this.save = (draft) => {
        //console.log(draft)
        this.tasks = this.tasks.map(t => t.id === draft.id ? draft : t)
        this.selected = draft
        this.mode = 'details' 
      }

      this.fetchTaskFromService = () => {
        TaskService.fetchTask().then( ()=> {
          this.selectedTasks = TaskService.getTasks()          
         })
      }

      this.select = (task) => {
         this.selected = task
         
        }
      this.cancelEdition = (event) => { this.mode = 'details'}
      this.cancelSelect = (event) => { this.selected = null; this.mode = 'details' }
      this.edit = (event) => { 
          this.mode = 'edit'
      }
    }
  })
  .component('tasksList', {
    bindings: { tasks: '=', onSelect: "&"},
    template:/* html */`<div> List: <div class="list-group">
        <div class="list-group-item" ng-repeat="task in $ctrl.tasks" 
          ng-class="{active: task.name == $ctrl.selected.name}"
          ng-click="$ctrl.select(task)">
        {{$index+1}}. {{task.name}}</div>
      </div></div>`,
    controller() {
      this.$onInit = () => {
        this.task
        this.selected
      }
      this.$onChanges = () => {
      }
      this.select = (task) => {
        this.selected = task
        this.onSelect({ $event: task })      
      };
      
    }
  })
  .component('taskDetails', {
    bindings: { task: '=', onEdit: '&', onCancel: '&' },
    template:/* html */`<div>tasksDetails <dl>
      <dt>Name</dt><dd>{{$ctrl.task.name}}</dd>
    </dl>
      <button ng-click="$ctrl.onEdit({$event: $ctrl.task})">Edit</button>
      <button ng-click="$ctrl.onCancel({$event:$ctrl.task})">Cancel</button>
    </div>`,
    controller($rootScope, $http) { 

    } // this == $ctrl
  })
  .component('taskEditor', {
    bindings: { task: '<', onEdit: '&', onCancel: '&', onSave: '&' },
    template:/* html */`<div> taskForm:
        <div class="form-group mb-3">
          <label for="task_name">Name</label>
          <input id="task_name" type="text" class="form-control" 
                ng-model="$ctrl.draft.name">
        </div>

        <div class="form-group mb-3">
          <label for="task_completed">
            <input id="task_completed" ng-model="$ctrl.draft.completed" type="checkbox"> Completed</label>
        </div>

        <button ng-click="$ctrl.onSave({$event: $ctrl.draft})">Save</button>
        <button ng-click="$ctrl.onCancel({$event:task, mode:'placki'})">Cancel</button>
      </div>`,
    // controller:'TaskEditorCtrl as $ctrl'
    controller($scope) {
      this.$onInit = () => {
        this.draft = { ...this.task }
      }      
      // https://docs.angularjs.org/guide/component#component-based-application-architecture
      this.$onChanges = (changes) => { console.log(changes) }
      this.$doCheck = () => { } // after parent $digest
      this.$postLink = () => { } // after all child DOM is ready
      this.on
      this.$onDestroy = () => {
        console.log('bye bye!')
      }
      this.save = () => {        
        this.onSave({$event: this.draft})
      }      
    },
    // controllerAs: '$ctrl'
  })