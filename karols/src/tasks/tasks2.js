const tasks = angular.module("tasks", []);

tasks.controller("TasksCtrl", ($scope) => {
  $scope.showForm = true;
  $scope.form = {
    id: "123",
    name: "Task 123",
    completed: false,
  };

  $scope.tasks = [
    {
      id: "123",
      name: "Task 123",
      completed: true,
    },
    {
      id: "234",
      name: "Task 234",
      completed: false,
    },
    {
      id: "345",
      name: "Task 345",
      completed: true,
    },
  ];

//   document.addEventListener("click", (event) => {
    // if (event.path.find((el) => el.matches && el.matches(".list-group-item")))
    //   return;

    // $scope.selectedTask = undefined;
    // console.log(event.target.dataset.listItemId)
    // if(event.target.dataset.listItemId==undefined){
    //     $scope.searchTask()
    //     $scope.$digest()
    //     return
    // }
    // $scope.activeListItem = event.target.dataset.listItemId

    // $scope.changeListActiveItem(event.target.dataset.listItemId)
    // $scope.searchTask()
    // $scope.$digest();
//   });

  $scope.displayedTaskList = $scope.tasks;

  $scope.changeListActiveItem = (id) => {
    $scope.showForm = true;
    //$scope.activeListItem = id
    const selectedItem = $scope.tasks.find((item) => item.id === id.toString());
    //console.log(selectedItem)
    $scope.selectedTask = { ...selectedItem };
  };
  $scope.filteringParameter = "";

  $scope.searchTask = () => {
    //console.log($scope.filteringParameter)
    //console.log($scope.tasks.filter( item => item.name.toLocaleLowerCase().includes($scope.filteringParameter.toLocaleLowerCase() )))
    $scope.displayedTaskList = $scope.tasks.filter((item) =>
      item.name
        .toLocaleLowerCase()
        .includes($scope.filteringParameter.toLocaleLowerCase())
    );
  };

  $scope.selectedTask = {
    id: "123",
    name: "Task 123",
    completed: true,
  };

  $scope.changeTaskStatus = () => {
    $scope.form.completed = !$scope.form.completed;
  };

  $scope.taskToAdd = {
    id: "",
    name: "",
    completed: false,
  }

  $scope.addTaskWithEnterKay = (keyEvent) => {
    if (keyEvent.which === 13)
    $scope.addTask()
  }

  $scope.addTask = () =>{
      if($scope.taskToAdd.name=== ""){return}
    $scope.taskToAdd.id = String(Number($scope.tasks[Object.keys($scope.tasks).length-1].id)+1)
    //console.log($scope.taskToAdd.id)
    $scope.tasks.push({
        ...$scope.taskToAdd
    })
    //console.log( $scope.tasks)
    $scope.searchTask();
  }

  $scope.setNewTaskStatus = () => {
    $scope.taskToAdd.completed = !$scope.taskToAdd.completed;
  };  

  $scope.editTask = (id) => {
    $scope.showForm = !$scope.showForm;
    $scope.form = { ...$scope.selectedTask };
    $scope.searchTask();
  };

  $scope.saveForm = (id) => {
    $scope.selectedTask = { ...$scope.form };
    $scope.showForm = !$scope.showForm;
    var oldTasksList = [...$scope.tasks];
    // console.log(oldTasksList)
    $scope.tasks = [];

    for (var tsk of oldTasksList) {
      if (tsk.id == id) {
        $scope.tasks.push($scope.form);
      } else {
        $scope.tasks.push(tsk);
      }
    }
    //console.log($scope.tasks)
    $scope.searchTask();
  };

  $scope.deleteItemOnTaskList = (id) => {
    $scope.tasks = $scope.tasks.filter((item) => item.id !== id.toString());
    $scope.searchTask;
    $scope.searchTask();
  };

  $scope.cancelForm = () => {
    $scope.showForm = !$scope.showForm;
    $scope.searchTask();
  };
});
