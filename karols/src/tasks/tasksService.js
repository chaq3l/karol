angular.module('task.service', []).service('TaskService', function($http){
    const URL = 'http://localhost:3000/tasks'

    this._tasks = []

    this.getTasks = () => {
        return this._tasks
    }

    this.sendTask = (task) => {
        return $http.post(URL, task)
        .then(res=>res.data)
    }

    this.fetchTask = () => {
       return $http.get(URL)        
        .then(data => this._tasks = data.data)        
    }

    this.deleteTask = (taskData) => {
        return $http.delete(URL+'/'+taskData.id.toString()+'/')
        .then(res=>res.data) 
    }

    this.editTask = (taskData) => {        
        return $http.put(URL+'/'+taskData.id.toString()+'/', taskData)
        .then(res=>res.data)       
    }
})