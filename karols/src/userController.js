angular.module('users', ['user.service'])
.controller('UsersPackageCtrl', ($scope, UserService) => {
    //$scope.fetchUsers
    var vm = $scope.usersPage = {}
    vm.selected = null
    vm.selectedUserId = null
    vm.edited = null
    vm.editMode = false
    

    vm.editUser = () => {
        vm.editMode =  !vm.editMode
    }

    vm.addNewUser = (name) =>
    {        
        UserService.addNewUser(name).then(vm.fetchUsers)
        //vm.$digest()
        //UserService.sendUser(name)
    }
    
    vm.deleteUser = (draft) => {
        console.log(vm.editionForm.$invalid)
        if(!vm.editionForm.$invalid){
            if( vm.edited )
            UserService.deleteUser(draft).then(vm.fetchUsers)
            vm.edited = null
            vm.edited = null
            vm.editMode =  !vm.editMode
        }
        
    }

    vm.select = (id) => 
    {
        vm.selected = UserService.getUsersById(id)
        vm.edited = UserService.getUsersById(id)
        vm.selectedUserId = id
    }

    vm.save = (draft) => {
        if( vm.edited )
        vm.select = UserService.updateUser(draft)
        vm.select = (draft.id)
        vm.select = UserService.getUsers()
        vm.edited = null
        vm.edited = null
    }

    vm.updateUser = (draft) => {
        if( vm.edited )
        UserService.updateUser(draft).then(vm.fetchUsers)
        vm.edited = null
        vm.edited = null
        vm.editMode =  !vm.editMode
    }
    
    vm.fetchUsers = async () => {
       await UserService.fetchUsers().then( () => {
        vm.users = UserService.getUsers()        
        })
      }

    vm.fetchUsers()
})
// .controller('SettingsPackage', ($scope, UserService) => {
// })

angular.module('user.service', []).service('UserService', function($http){
//angular.module('user.service', []).service('UsersService', function(){
    this._users = []

    this.updateUser = (draft) => {
        console.log(draft)
        //this._users = this._users.map( user => { return user.id === draft.id.toString() ? draft : user })
       return this.editUser(draft)        
    }

    this.addNewUser = (name) => {
        const checkIfUserAlreadyExist = this._users.find( user => user.username === name.toString())
        //console.log(checkIfUserAlreadyExist)        
        
        return this.sendUser( 
            {                
            id: String(Number(this._users[Object.keys(this._users).length-1].id)+1),
            username: name.toString()
            })
    }
    

    this.getSelectUser = null

    this.getUsersById = (id) =>
    { 
        this.selectUser = this._users.find( user => user.id === id.toString() )

        //console.log(this._users.find( user => user.id === id.toString()))
        return { ...this.selectUser}
    }    
    this.getUsers = () => {
        return this._users
    }

    this.sendUser = (user) => {
        return $http.post('http://localhost:3000/users', user)
        .then(res=>res.data)
    }

    this.fetchUsers = () => {
       return $http.get('http://localhost:3000/users')        
        .then(data => this._users = data.data)        
        //$rootScope.$digest()        
    }

    this.deleteUser = (userData) => {
        return $http.delete('http://localhost:3000/users/'+userData.id.toString()+'/')
        .then(res=>res.data) 
    }

    this.editUser = (userData) => {        
        return $http.put('http://localhost:3000/users/'+userData.id.toString()+'/', userData)
        .then(res=>res.data)       
    }

})