
angular.module('app', [])


angular.module('app').controller('ticTacToe', ($scope) => {
    $scope.doFnWhenConditionIsFalse = (condition, fn) => {
        if(!condition){
            fn
        }
    }
    $scope.turn = "X"
    $scope.boardSize = 3
    $scope.numberToWin = 3
    $scope.board = []
    $scope.invertedArray = []
    $scope.winClime = false
    
    $scope.nextTurn = () => {
        $scope.turn = $scope.turn === "X"? "O" : "X"
    }
    
    $scope.createBoard = () => {
        //$scope.numberToWin = $scope.boardSize
        $scope.turn = "X"
        $scope.invertedArray = []
        if($scope.boardSize<$scope.numberToWin){
            return
        }
        $scope.board = []
        for(var i = 0; i < $scope.boardSize; i++){
            row = []
            for(var j = 0; j < $scope.boardSize; j++)
            {
                row.push({
                    markPlace: ""
                    // marjPlace: '&nbsp;'
                })
            }
            $scope.board.push(
            {object:{ rowIndex: i,  row  }})
        }

        for(var i = $scope.boardSize; i>0; i--){
            //console.log(i)
            $scope.invertedArray.push(-i)            
        }
        $scope.winClime = false
       //console.log($scope.invertedArray)
        
    }

    $scope.markPosition = (indexRow, indexCol)=>{
        if($scope.board[indexRow].object.row[indexCol].markPlace === ""){            
            $scope.board[indexRow].object.row[indexCol].markPlace = $scope.turn
            $scope.nextTurn()
            $scope.winConfirmation()            
        }        
    }

    $scope.winConfirmation = () =>{
        $scope.doFnWhenConditionIsFalse($scope.winClime, $scope.columnConfirmation())
        $scope.doFnWhenConditionIsFalse($scope.winClime, $scope.rowConfirmation())
        $scope.doFnWhenConditionIsFalse($scope.winClime, $scope.crossConfirmation1())
        $scope.doFnWhenConditionIsFalse($scope.winClime, $scope.crossConfirmation2())
        $scope.doFnWhenConditionIsFalse($scope.drawDetection())
        if($scope.winClime){
            $scope.newGame()
        }
    }

    $scope.winAlert = async(message) =>
    {
       await window.alert(message)
    }

    $scope.rowConfirmation = async() =>{
        for (row of $scope.board){            
            var winConfirmationFeedback=""   
            for(var a = 0; a < $scope.boardSize; a++){                
                winConfirmationFeedback += row.object.row[a].markPlace                
            }
            if(winConfirmationFeedback==="X".repeat($scope.numberToWin) || winConfirmationFeedback==="O".repeat($scope.numberToWin)){                
                window.alert(winConfirmationFeedback.slice(0,1)+ " win")
                $scope.winClime = true
            }
                
        }
    }

    $scope.columnConfirmation = () =>{
        for (var x = 0; x < $scope.boardSize; x++){
            var winConfirmationFeedback=""   
            for(var y = 0; y < $scope.boardSize; y++){                
                winConfirmationFeedback += $scope.board[y].object.row[x].markPlace 
                if(winConfirmationFeedback==="X".repeat($scope.numberToWin) || winConfirmationFeedback==="O".repeat($scope.numberToWin)){                
                    window.alert(winConfirmationFeedback.slice(0,1)+ " win")
                    $scope.winClime = true
                }
            }
            //console.log( winConfirmationFeedback )     
        }
    }

    $scope.crossConfirmation1 = () =>{
        for (var x = 0; x <= $scope.boardSize - $scope.numberToWin; x++){
            for (var y = 0; y <= $scope.boardSize - $scope.numberToWin; y++){
                var winConfirmationFeedback=""
                for(var z = 0; z < $scope.numberToWin; z++){                
                    winConfirmationFeedback += $scope.board[x+z].object.row[y+z].markPlace                        
                    if(winConfirmationFeedback==="X".repeat($scope.numberToWin) || winConfirmationFeedback==="O".repeat($scope.numberToWin)){                
                        window.alert(winConfirmationFeedback.slice(0,1)+ " win")
                        $scope.winClime = true
                        break
                    }                    
                }
            }  
        }
    }

    $scope.crossConfirmation2 = () =>{        
        for (var x = $scope.boardSize-1; x >=  $scope.numberToWin-1; x--){            
            for (var y = 0; y <= $scope.boardSize - $scope.numberToWin; y++){                
                var winConfirmationFeedback=""
                for(var z = 0; z < $scope.numberToWin; z++){                
                    winConfirmationFeedback += $scope.board[y+z].object.row[x-z].markPlace 
                    //do skończenia i sprawdzenia                       
                        if(winConfirmationFeedback==="X".repeat($scope.numberToWin) || winConfirmationFeedback==="O".repeat($scope.numberToWin)){                
                            window.alert(winConfirmationFeedback.slice(0,1)+ " win")
                            $scope.winClime = true
                            break
                        }                    
                    }
                    
            }  
        }
    }

    $scope.drawDetection = () => {
        var drawConfirmationFeedback=""   
        for (var x = 0; x < $scope.boardSize; x++){
            for(var y = 0; y < $scope.boardSize; y++){                
                drawConfirmationFeedback += $scope.board[y].object.row[x].markPlace 
                if(drawConfirmationFeedback.length === $scope.boardSize*$scope.boardSize){      
                   // console.log(drawConfirmationFeedback)          
                    window.alert("Draw")
                    $scope.newGame()
                }
            }
            //console.log( drawConfirmationFeedback )     
        }
    }
    
    $scope.newGame = ()=>{
        $scope.turn = "X"
        $scope.board = []
    }

})