angular.module('settings', [])
  .controller('SettingsCtrl', ($scope) => {
    const vm = $scope.settings = {}

    vm.settingA = { title: 'Setting A', value: 'ABC' }
    vm.settingB = { title: 'Setting B', value: '123' }
    vm.settingC = { title: 'Setting C', value: 'XYZ' }

    vm.user = { name: 'Test', email: 'test@test' }
  })


angular.module('settings').directive('userDetails', function () {
  return {
    scope: {
      buttonLabel: '@btnLabel', // scope.buttonLabel = attr.btnLabel
      user: '=user', // scope.$watch(attr.user),
      //user: '<user', // scope.$watch(attr.user), // = i > jedno robi jednostronnie a drugie obustronnie
      editFn: '&onEdit' // editFn = () => scope.$parent.$eval(attr.onEdit, arg)
    },
    template: /* html */`<div class="border p-4">
    <dl>
     <dt>Name</dt>
      <dd>{{user.name}}</dd>
      <dt>E-mail</dt>
      <dd>{{user.email}}</dd>
      </dl>
      <input ng-model="user.name">
    <button ng-click="editFn({$event:user})">
      {{buttonLabel}}
    </button></div>`
  }

})


angular.module('settings').directive('appEditField', function () {

  return {
    scope: true, // create child scope
    template:/* html */`<div class="form-group">
      <label ng-click=" edit = !edit ">{{title}} </label>
      
      <input  ng-show="edit  " ng-model="displayValue" ng-keyup="$event.key == 'Enter' && save()">
      <span  ng-show="!edit">{{displayValue}}</span>
    </div>`,
    link(scope, $element, attr, ctrl) {
      // <app-edit-field item-label="Alice" 
      scope.title = attr.itemLabel  // scope: {title: '@itemLabel' }

      // scope.$watch(' settings.settingA.value.toString() ', (newVal, oldVal) => {
      scope.$parent.$watch(attr.parentVal, (newVal, oldVal) => {
        scope.displayValue = newVal
      })
      scope.save = () => {
        console.log(scope.title, scope.displayValue)
        //  on-save=" settings.settingA.value = innerPlacki"
        scope.$parent.$eval(attr.onSave, {
          innerPlacki: scope.displayValue,
          $event: scope.displayValue
        })
        scope.edit = false
        // $scope.$parent. settings.settingA.value = $scope.displayValue
      }
    },
    controller($scope) {
      $scope.edit = false;
    }
  }
})





// <app-highlight>
angular.module('settings').directive('appHighlight', function (
  /* dependencies... PAGES,$q, $timeout, ... */
) {
  return {
    restrict: 'EACM', // Element | Attr | Class | coMment
    link(scope, $element, attrs, ctrl) {
      console.log('Hello', $element);

      scope.$watch('settings.title', () => {
        $element
          .html('<p>' + scope.settings.title + ' &raquo; &nbsp; </p>')
      })

      $element
        .css({ 'color': 'red' })
        // .after('<p>placki &raquo; </p>')
        .html('<p>' + scope.settings.title + ' {{ &raquo; }} </p>')
        .on('click', event => {
          // angular.element(event.currentTarget)
          $element
            .find('p')[0].classList.toggle('text-success')
          // .css('color', 'blue')
          scope.settings.title = 'Clicked'
          scope.$digest()
        })
    }
  }
})

// /** @type {require('angular').IDirectiveFactory} */